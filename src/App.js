import "./App.css";
import React from "react";
import Card from "./components/Card.js";

class App extends React.Component {
  state = {
    card: [
      {
        id: 1,
        cardTag: "DEV",
        cardTitle: "Learning React? Start Small.",
        cardAuthor: "Dave Ceddia",
        cardAuthorImage:
          "https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8cmFuZG9tJTIwcGVvcGxlfGVufDB8fDB8fA%3D%3D&w=1000&q=80",
        cardDescription:
          "Can't pry youself away from the tutorials? The cure is to make tiny little experiment apps.",
      },
      {
        id: 2,
        cardTag: "TESTING",
        cardTitle: "Learn Selenium with me.",
        cardAuthor: "Sumen Dubey",
        cardAuthorImage:
          "https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8cmFuZG9tJTIwcGVvcGxlfGVufDB8fDB8fA%3D%3D&w=1000&q=80",
        cardDescription:
          "Don't wait anymore, come and learn Testing with Selenium with me.",
      },
      {
        id: 3,
        cardTag: "TOURISM",
        cardTitle: "Explore Karnataka",
        cardAuthor: "Mayank Dubey",
        cardAuthorImage:
          "https://images.unsplash.com/photo-1499996860823-5214fcc65f8f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Nnx8cmFuZG9tJTIwcGVyc29ufGVufDB8fDB8fA%3D%3D&w=1000&q=80",
        cardDescription: "Karnataka - One State, Many World's",
      },
      {
        id: 4,
        cardTag: "DEFENCE",
        cardTitle: "Indian Air Force - ISRO Collaboration",
        cardAuthor: "Somu Kumar",
        cardAuthorImage:
          "https://qph.cf2.quoracdn.net/main-qimg-134e3bf89fff27bf56bdbd04e7dbaedf.webp",
        cardDescription:
          "Indian Air Force - ISRO Collaboration for Country's project.",
      },
      {
        id: 5,
        cardTag: "FASHION",
        cardTitle: "Priyanka's New Dress",
        cardAuthor: "Sumona Chatterjee",
        cardAuthorImage:
          "https://static.skillshare.com/cdn-cgi/image/quality=80,width=1000,format=auto/uploads/project/4c49440fec9b45c5464596033e0a23d7/ad168d89",
        cardDescription:
          "Priyanka spotted with her new dress at Mumbai International Airport.",
      },
      {
        id: 6,
        cardTag: "MOVIES",
        cardTitle: "Avatar: The Way of Water Released",
        cardAuthor: "Dilkash Khan",
        cardAuthorImage:
          "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/3f01db52-f675-48dc-9c91-f245d99f1486/d2nqynw-af694fd2-e1ba-4e9c-badb-630a48474599.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzNmMDFkYjUyLWY2NzUtNDhkYy05YzkxLWYyNDVkOTlmMTQ4NlwvZDJucXludy1hZjY5NGZkMi1lMWJhLTRlOWMtYmFkYi02MzBhNDg0NzQ1OTkuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.59_LN0TnrsDrVLS266jLpfZZfte_OZeNGkNQFJzgQCM",
        cardDescription:
          "Avatar: The Way of Water is a 2022 American epic science fiction film directed and produced by James Cameron.",
      },
    ],
  };
  render() {
    return (
      <div>
        <nav>Social Cards</nav>
        <Card cardData={this.state.card} />
        <footer>The End</footer>
      </div>
    );
  }
}

export default App;
