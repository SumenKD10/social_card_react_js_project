import React, { Component } from "react";

class CardAuthor extends Component {
  render() {
    return (
      <div className="cardAuthor">
        <h3>{this.props.cardAuthorDetail}</h3>
        <img src={this.props.cardAuthorImage} alt="" className="authorImage" />
      </div>
    );
  }
}

export default CardAuthor;
