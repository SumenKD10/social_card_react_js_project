import React, { Component } from "react";

class CardTag extends Component {
  render() {
    return (
      <div className="cardTag">
        <h3>{this.props.cardTagDetail}</h3>
      </div>
    );
  }
}

export default CardTag;
