import React, { Component } from "react";

class CardTitle extends Component {
  render() {
    return (
      <div className="cardTitle">
        <h1>{this.props.cardTitleDetail}</h1>
      </div>
    );
  }
}

export default CardTitle;
