import React, { Component } from "react";

class CardDetails extends Component {
  render() {
    return (
      <div className="cardDetails">
        <h4>{this.props.cardTitleDetail}</h4>
        <p>{this.props.cardDesDetail}</p>
      </div>
    );
  }
}

export default CardDetails;
