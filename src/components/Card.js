import React, { Component } from "react";
import CardTag from "./CardTag.js";
import CardTitle from "./CardTitle.js";
import CardAuthor from "./CardAuthor.js";
import CardDetails from "./CardDetails.js";

class Cards extends Component {
  render() {
    return this.props.cardData.map((eachCard) => {
      return (
        <div className="card" key={eachCard.id}>
          <div className="imageContainer">
            <CardTag cardTagDetail={eachCard.cardTag} />
            <CardTitle cardTitleDetail={eachCard.cardTitle} />
            <CardAuthor
              cardAuthorDetail={eachCard.cardAuthor}
              cardAuthorImage={eachCard.cardAuthorImage}
            />
          </div>
          <CardDetails
            cardTitleDetail={eachCard.cardTitle}
            cardDesDetail={eachCard.cardDescription}
          />
        </div>
      );
    });
  }
}


export default Cards;
